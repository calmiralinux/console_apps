#!/bin/bash -e

export PATH=$PATH:/opt/rustc/bin
cd src

for project in * ; do
  cd $project
  make build.release
  make install
  cd -
done
cd ..

cd data/configs
make
cd ../../

mkdir /usr/share/doc/calmira -pv
cp -rv data/tips /usr/share/doc/calmira
