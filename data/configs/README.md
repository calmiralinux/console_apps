# Конфигурационные файлы Calmira GNU/Linux-libre

## Config list

- `/etc`:
    - `profile`;
    - `bashrc`;
    - `issue`;
    - `inittab`;
    - `os-release`;
    - `lsb-release`.
- `/etc/skel`:
    - `.bash_profile`;
    - `.bashrc`;
    - `.bash_logout`;
- `/etc/profile.d`
    - `bash_completion.sh`;
    - `dircolors.sh`;
    - `readline.sh`;
    - `umask.sh`;
    - `locale.sh`;
