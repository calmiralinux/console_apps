# Begin /etc/profile
# Copyright (C) 2022 Michail Krasnov <linuxoid85@gmail.com>

# Functions to help us manage paths.  Second argument is the name of the
# path variable to be modified (default: PATH)
pathremove () {
        local IFS=':'
        local NEWPATH
        local DIR
        local PATHVARIABLE=${2:-PATH}
        for DIR in ${!PATHVARIABLE} ; do
                if [ "$DIR" != "$1" ] ; then
                  NEWPATH=${NEWPATH:+$NEWPATH:}$DIR
                fi
        done
        export $PATHVARIABLE="$NEWPATH"
}

pathprepend () {
        pathremove $1 $2
        local PATHVARIABLE=${2:-PATH}
        export $PATHVARIABLE="$1${!PATHVARIABLE:+:${!PATHVARIABLE}}"
}

pathappend () {
        pathremove $1 $2
        local PATHVARIABLE=${2:-PATH}
        export $PATHVARIABLE="${!PATHVARIABLE:+${!PATHVARIABLE}:}$1"
}

export -f pathremove pathprepend pathappend

# Set the initial PATH
export PATH="/bin:/usr/bin"

if [ $EUID -eq 0 ]; then
    pathappend /sbin
    pathappend /usr/sbin
fi

# Set some defaults for graphical systems
export XDG_DATA_DIRS=${XDG_DATA_DIRS:-/usr/share/}
export XDG_CONFIG_DIRS=${XDG_CONFIG_DIRS:-/etc/xdg/}
export XDG_RUNTIME_DIR=${XDG_RUNTIME_DIR:-/tmp/xdg-$USER}

# Setup a command prompt
function get_git() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

PS1="? " # Default prompt
if [[ $EUID == 0 ]]; then
    PS1="\u:\W# "
else
    if [ -f "/bin/git" ]; then
      PS1="\u:\w\e[33m$(get_git)\e[0m\n% "
    else
      PS1="\u:\W% "
    fi
fi

for script in /etc/profile.d/*.sh; do
    if [ -r $script ]; then
        . $script
    fi
done

unset script

calm-tips

#End /etc/profile
