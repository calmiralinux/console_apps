# Begin ~/.bash_profile
# Copyright (C) 2022 Michail Krasnov <linuxoid85@gmail.com>

if [ -f "$HOME/.bashrc" ]; then
    source $HOME/.bashrc
fi

if [ -d "$HOME/.local/bin" ]; then
    pathprepend $HOME/.local/bin
fi

# End ~/.bash_profile
