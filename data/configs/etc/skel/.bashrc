# Begin ~/.bashrc
# Copyright (C) 2022 Michail Krasnov <linuxoid85@gmail.com>

if [ -f /etc/bashrc ]; then
    source /etc/bashrc
fi

# Set up user specific i18n variables
#export LANG=ru_RU.UTF-8

# End ~/.bashrc
