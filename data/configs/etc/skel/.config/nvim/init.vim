" Begin ~/.config/nvim/init.vim

set nu
set lbr
set mouse=a
set encoding=utf-8

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set textwidth=80
set cursorline

set fileformat=unix
filetype indent on

" End ~/.config/nvim/init.vim
