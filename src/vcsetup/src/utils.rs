/*!
Модуль со вспомогательными функциями.
*/
use std::path::Path;

use walkdir:: WalkDir;

/// Тип данных которые вернет 'get_dir_content'
pub enum Content {
    /// Обычные файлы
    File,
    /// Директории
    Dir,
    /// Симлинки
    Link,
}

/// Рекурсивно сканирует выбранную директорию.
///
/// Параметры:
/// * ctype:   тип данных
/// * path:    Полный путь до директории
/// * pattern: Расширение файлов которые будут возвращены
///
/// # Example
///
/// ```ignore
/// let files = get_dir_content(Content::File, "/usr/share/kbd/consolefonts/", "gz");
/// ```
pub fn get_dir_content(ctype: Content, path: &str, pattern: &str) -> Vec<String> {
    let mut content: Vec<String> = Vec::new();

    // Receive iterator over all items in the path
    let walker = WalkDir::new(path).into_iter();
    // Iterate over and ignore all errors
    for entry in  walker.filter_map(|e| e.ok()) {
        match ctype {
            Content::File => {
                if entry.file_type().is_file() && entry.path().extension().unwrap_or_default().to_str() == Some(pattern) {
                    content.push(trim_file(entry.path()))
                }
            },
            Content::Dir => {
                if entry.file_type().is_dir() {
                    content.push(trim_file(entry.path()))
                }
            },
            Content::Link => {
                if entry.file_type().is_symlink() {
                    content.push(trim_file(entry.path()))
                }
            }
        }
    }

    content
}

/// Возвращает имя файла без расширения
pub fn trim_extention(file: &str) -> String {
    let file_name = Path::new(file).file_stem().unwrap_or_default();

    format!("{}", file_name.to_string_lossy())
}

/// Принимает полный путь к файлу и возвращает имя файла без пути.
///
/// Возвращает имя файла или входящее значение без изменений в случае ошибки.
fn trim_file(file: &Path) -> String {
    // let font = match Path::new(file).file_name() {
    //     Some(p) => p,
    //     // Return back not changed file
    //     None => return file.to_owned(),
    // };
    let font = file.file_name().unwrap_or_default();

    let fname = font.to_str().unwrap_or("");

    String::from(fname)
}