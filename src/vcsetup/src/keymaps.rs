
use crate::utils::*;

/*
Принцип работы:

- Получение списка файлов из `/usr/share/keymaps`
- Добавление файлов с расширением *.gz в меню в качестве пунктов со шрифтами
- Добавление поддиректорий с файлами *.gz в качестве подменю
- Добавление файлов *.gz из тех поддиректорий в подменю в качестве пунктов со шрифтами
- Игнорирование директорий `include` и файлов, не оканчивающихся на *.gz
 */

pub fn get_keymaps() -> Vec<String> {
    get_dir_content(Content::File, "/usr/share/keymaps/", "gz")
        .into_iter()
        // Remove .gz
        .map(|file| trim_extention(&file))
        // Remove .map
        .map(|file| trim_extention(&file))
        .collect()
}
