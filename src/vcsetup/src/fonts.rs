
use crate::utils::*;

/*
Принцип работы:

- Получение списка файлов из `/usr/share/kbd/consolefonts`
- Добавление файлов с расширением *.gz в меню в качестве пунктов со шрифтами
- Добавление поддиректорий с файлами *.gz в качестве подменю
- Добавление файлов *.gz из тех поддиректорий в подменю в качестве пунктов со шрифтами
- Игнорирование файлов, не оканчивающихся на *.gz
 */

pub fn get_fonts() -> Vec<String> {
    get_dir_content(Content::File, "/usr/share/consolefonts/", "gz")
        .into_iter()
        // Remove .gz
        .map(|file| trim_extention(&file))
        .collect()
}
