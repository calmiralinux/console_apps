
use std::path::PathBuf;

use cursive::Cursive;
use cursive::event::Key;
use vcsetup::{ui, constants};
use vcsetup::config::{Config, ConfigPath};

fn main() {
    let mut screen = cursive::default();

    screen.add_global_callback('q', Cursive::quit);
    screen.add_global_callback(Key::Esc, |s| ui::on_show_main(s));
    screen.set_user_data(
        Config::parse_conf(&ConfigPath(PathBuf::from(constants::CONFIG_PATH))).unwrap_or_default()
    );

    ui::menubar(&mut screen);
    ui::show_main_window(&mut screen);

    screen.run();
}
