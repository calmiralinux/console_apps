/*!
 Модуль интерфейса.
 */
use std::path::PathBuf;

use cursive::Cursive;
use cursive::menu;
use cursive::align::HAlign;
use cursive::event::{EventResult, Key};
use cursive::theme::{Color, PaletteColor, load_default};
use cursive::view::{Scrollable, Resizable};
use cursive::views::{
    Dialog, SelectView,
    TextView, OnEventView,
    LinearLayout, DummyView,
    Panel,
};

use crate::config::{Config, ConfigPath, LogLevel, Lang, LClang, Font, KeyMap};
use crate::fonts::get_fonts;
use crate::keymaps::get_keymaps;
use crate::languages::get_languages;
use crate::constants;

/// Меню с дополнительными действиями
pub fn menubar(screen: &mut Cursive) {
    screen.menubar()
        .add_subtree(
            "Help",
            menu::Tree::new()
                .leaf("About", |s| show_about_win(s))
                .leaf("Shortcuts", |s| show_shortcuts_win(s))
                .leaf("Online docs", |s| show_docs_win(s))
        );

    screen.set_autohide_menu(false);
    screen.add_global_callback(Key::F1, |s| s.select_menubar());
}

fn show_about_win(screen: &mut Cursive) {
    let about = Dialog::around(TextView::new(
        format!("vcsetup v{}. Program for setting up virtual consoles (TTY) \
        in Calmira GNU/Linux-libre system.\n\n(C) 2023 Calmira GNU/Linux-libre \
        Developers", constants::VERSION))
    )
    .title("About vcsetup")
    .button("Ok", on_show_main);

    screen.pop_layer();
    screen.add_layer(about);
}

fn show_shortcuts_win(screen: &mut Cursive) {
    let shorts = Dialog::around(TextView::new(constants::SHORTS))
        .title("Keyboard shortcuts")
        .button("Ok", on_show_main);

    screen.pop_layer();
    screen.add_layer(shorts);
}

fn show_docs_win(screen: &mut Cursive) {
    let docs = Dialog::around(
        TextView::new(
            format!(
                "Documentation portal: {}\
                \nCalmira HandBook:     {}",
                &constants::DOC_REPO, &constants::HBOOK,
            )
        )
    )
    .title("Online docs")
    .button("Ok", on_show_main);

    screen.pop_layer();
    screen.add_layer(docs);
}

/// Показывает гланое окно программы
pub fn show_main_window(screen: &mut Cursive) {
    // Set default colors
    screen.set_theme(load_default());
    let mut select = SelectView::new()
        // Center the text horizontally
        .h_align(HAlign::Left)
        // Use keyboard to jump to the pressed letters
        .autojump();
    // Sets the callback for when "Enter" is pressed.
    select.set_on_submit(on_selected_main);

    // Get values from Config
    let loglevel = screen.user_data::<Config>()
        .unwrap_or(&mut Config::default())
        .loglevel.as_ref()
        .unwrap_or(&LogLevel::default())
        .to_string();

    let language = screen.user_data::<Config>()
        .unwrap_or(&mut Config::default())
        .lang.as_ref()
        .unwrap_or(&Lang::default())
        .to_string();

    let lc_language = screen.user_data::<Config>()
        .unwrap_or(&mut Config::default())
        .lc_lang.as_ref()
        .unwrap_or(&LClang::default())
        .to_string();

    let font = screen.user_data::<Config>()
        .unwrap_or(&mut Config::default())
        .font.as_ref()
        .unwrap_or(&Font::default())
        .to_string();

    let keymap = screen.user_data::<Config>()
        .unwrap_or(&mut Config::default())
        .keymap.as_ref()
        .unwrap_or(&KeyMap::default())
        .to_string();

    select.add_item(format!("Set system language:   {}", language), 1);
    select.add_item(format!("Set locale:            {}", lc_language), 2);
    select.add_item(format!("Set console font:      {}", font), 3);
    select.add_item(format!("Set console keymap:    {}", keymap), 4);
    select.add_item(format!("Set system loglevel:   {}", loglevel), 5);

    // Let's override the `j` and `k` keys for navigation
    let select = OnEventView::new(select)
        .on_pre_event_inner('k', |s, _| {
            let cb = s.select_up(1);
            Some(EventResult::Consumed(Some(cb)))
        })
        .on_pre_event_inner('j', |s, _| {
            let cb = s.select_down(1);
            Some(EventResult::Consumed(Some(cb)))
        });

    screen.add_layer(
        Dialog::new()
            .title("Main menu")
            .content(
                Panel::new(select)
            )
            .button("Default", on_default)
            .button("Save", on_save)
            .button("Quit", |s| s.quit())
    );
}

// Sow main window when will return from another window
pub fn on_show_main(screen: &mut Cursive) {
    screen.pop_layer();
    show_main_window(screen);
}

// Callback for Save button
fn on_save(screen: &mut Cursive) {
    let path = PathBuf::from(constants::CONFIG_PATH);
    let mut msg = String::new();

    let mut theme = screen.current_theme().clone();

    screen.with_user_data(|cfg: &mut Config| {
        match cfg.write_conf(&ConfigPath(path.clone())) {
            Ok(_) => msg = format!("{} saved successfully", &path.to_string_lossy()),
            Err(why) => {
                theme.palette[PaletteColor::View] = Color::Rgb(240, 128, 128);
                msg = format!("Cannot write to {}:\n{}", &path.to_string_lossy(), why.to_string())
            },
        }
    });

    screen.set_theme(theme);
    screen.pop_layer();
    screen.add_layer(
        Dialog::around(TextView::new(msg)).button("Ok", on_show_main),
    );
}

// Callback for Default button
fn on_default(screen: &mut Cursive) {
    screen.with_user_data(|cfg: &mut Config| {
        cfg.lang = Some(Lang::default());
        cfg.font = Some(Font::default());
        cfg.keymap = Some(KeyMap::default());
        cfg.loglevel = Some(LogLevel::default());
    });
    // Redraw main screen
    screen.pop_layer();
    show_main_window(screen);
}

// Callback for selection in the main window
fn on_selected_main(screen: &mut Cursive, selected: &i32) {
    screen.pop_layer();
    match selected {
        1 => show_language_window(screen),
        2 => show_lc_language_window(screen),
        3 => show_fonts_window(screen),
        4 => show_keymap_window(screen),
        5 => show_loglevel_window(screen),
        _ => show_main_window(screen),
    }
}

// Show window with system languages selection
fn show_language_window(screen: &mut Cursive) {
    let mut select = SelectView::new()
        // Center the text horizontally
        .h_align(HAlign::Left)
        // Use keyboard to jump to the pressed letters
        .autojump();

    select.set_on_submit(on_selected_language);
    select.add_all_str(get_languages());

    // Let's override the `j` and `k` keys for navigation
    let select = OnEventView::new(select)
        .on_pre_event_inner('k', |s, _| {
            let cb = s.select_up(1);
            Some(EventResult::Consumed(Some(cb)))
        })
        .on_pre_event_inner('j', |s, _| {
            let cb = s.select_down(1);
            Some(EventResult::Consumed(Some(cb)))
        });

    screen.add_layer(
        Dialog::around(
            LinearLayout::vertical()
                .child(TextView::new("Select the language to be used\nfor the installed system.").h_align(HAlign::Center))
                .child(DummyView.fixed_height(1))
                .child(Panel::new( select.scrollable().fixed_size( (40,20) )) )
            )
            .title("Select system language")
            .button("Cancel", on_show_main),
    );
}

// Callback for selection in the language window
fn on_selected_language(screen: &mut Cursive, selected: &String) {
    screen.with_user_data(|cfg: &mut Config| {
        cfg.lang = Some(Lang::new(selected.to_owned()));
    });

    screen.pop_layer();
    show_main_window(screen);
}

// Show window with locales selection
fn show_lc_language_window(screen: &mut Cursive) {
    let mut select = SelectView::new()
        // Center the text horizontally
        .h_align(HAlign::Left)
        // Use keyboard to jump to the pressed letters
        .autojump();

    select.set_on_submit(on_selected_lc_language);
    select.add_all_str(get_languages());

    // Let's override the `j` and `k` keys for navigation
    let select = OnEventView::new(select)
        .on_pre_event_inner('k', |s, _| {
            let cb = s.select_up(1);
            Some(EventResult::Consumed(Some(cb)))
        })
        .on_pre_event_inner('j', |s, _| {
            let cb = s.select_down(1);
            Some(EventResult::Consumed(Some(cb)))
        });

    screen.add_layer(
        Dialog::around(
            LinearLayout::vertical()
                .child(TextView::new("Select the language to be used for currency,\ntime/date formatting etc.").h_align(HAlign::Center))
                .child(DummyView.fixed_height(1))
                .child(Panel::new(select.scrollable().fixed_size( (40,20) )) )
            )
            .title("Select locales")
            .button("Cancel", on_show_main),
    );
}

// Callback for selection in the language window
fn on_selected_lc_language(screen: &mut Cursive, selected: &String) {
    screen.with_user_data(|cfg: &mut Config| {
        cfg.lc_lang = Some(LClang::new(selected.to_owned()));
    });

    screen.pop_layer();
    show_main_window(screen);
}

// Show window with console fonts selection
fn show_fonts_window(screen: &mut Cursive) {
    let mut select = SelectView::new()
        // Center the text horizontally
        .h_align(HAlign::Left)
        // Use keyboard to jump to the pressed letters
        .autojump();

    select.set_on_submit(on_selected_font);
    select.add_all_str(get_fonts());
    // Let's override the `j` and `k` keys for navigation
    let select = OnEventView::new(select)
    .on_pre_event_inner('k', |s, _| {
        let cb = s.select_up(1);
        Some(EventResult::Consumed(Some(cb)))
    })
    .on_pre_event_inner('j', |s, _| {
        let cb = s.select_down(1);
        Some(EventResult::Consumed(Some(cb)))
    });

    screen.add_layer(
        Dialog::around(
        LinearLayout::vertical()
            .child(TextView::new("Select the font to be used for the console.").h_align(HAlign::Center))
            .child(DummyView.fixed_height(1))
            .child(Panel::new(select.scrollable().fixed_size((40,20) )) )
            )
            .title("Select console font")
            .button("Cancel", on_show_main),
    );
}

// Callback for selection in the console fonts window
fn on_selected_font(screen: &mut Cursive, selected: &String) {
    screen.with_user_data(|cfg: &mut Config| {
        cfg.font = Some(Font::new(selected.to_owned()));
    });
    // Redraw main screen
    screen.pop_layer();
    show_main_window(screen);
}

// Show window with console keymap selection
fn show_keymap_window(screen: &mut Cursive) {
    let mut select = SelectView::new()
        // Center the text horizontally
        .h_align(HAlign::Left)
        // Use keyboard to jump to the pressed letters
        .autojump();

    select.set_on_submit(on_selected_keymap);
    select.add_all_str(get_keymaps());
    // Let's override the `j` and `k` keys for navigation
    let select = OnEventView::new(select)
    .on_pre_event_inner('k', |s, _| {
        let cb = s.select_up(1);
        Some(EventResult::Consumed(Some(cb)))
    })
    .on_pre_event_inner('j', |s, _| {
        let cb = s.select_down(1);
        Some(EventResult::Consumed(Some(cb)))
    });

    screen.add_layer(
        Dialog::around(
        LinearLayout::vertical()
            .child(TextView::new("Select the keymap to use.").h_align(HAlign::Center))
            .child(DummyView.fixed_height(1))
            .child(Panel::new(select.scrollable().fixed_size( (40,20) )) )
        )
            .title("Select console keymap")
            .button("Cancel", on_show_main),
    );
}

// Callback for selection in the console keymaps window
fn on_selected_keymap(screen: &mut Cursive, selected: &String) {
    screen.with_user_data(|cfg: &mut Config| {
        cfg.keymap = Some(KeyMap::new(selected.to_owned()));
    });
    // Redraw main screen
    screen.pop_layer();
    show_main_window(screen);
}

// Show window with the log level selection
fn show_loglevel_window(screen: &mut Cursive) {
    let mut levels = SelectView::new()
        // Center the text horizontally
        .h_align(HAlign::Left)
        // Use keyboard to jump to the pressed letters
        .autojump();

    // Sets the callback for when "Enter" is pressed.
    levels.set_on_submit(on_selected_level);

    levels.add_item("0: Kernel Emergency", 0);
    levels.add_item("1: Kernel Alert", 1);
    levels.add_item("2: Kernel Critical", 2);
    levels.add_item("3: Kernel Error", 3);
    levels.add_item("4: Kernel Warning", 4);
    levels.add_item("5: Kernel Notice", 5);
    levels.add_item("6: Kernel Info", 6);
    levels.add_item("7: Kernel Debug", 7);

    // Let's override the `j` and `k` keys for navigation
    let levels = OnEventView::new(levels)
        .on_pre_event_inner('k', |s, _| {
            let cb = s.select_up(1);
            Some(EventResult::Consumed(Some(cb)))
        })
        .on_pre_event_inner('j', |s, _| {
            let cb = s.select_down(1);
            Some(EventResult::Consumed(Some(cb)))
        });

    screen.add_layer(
        Dialog::around(
            LinearLayout::vertical()
            .child(TextView::new("Select level for kernel-related messages\nretrieved from the kernel.").h_align(HAlign::Center))
            .child(DummyView.fixed_height(1))
            .child(Panel::new(levels))
        )
        .title("Select system loglevel")
        .button("Cancel", on_show_main),
    );
}

// Callback for selection in the log levels window
fn on_selected_level(screen: &mut Cursive, selected: &u8) {
    screen.with_user_data(|cfg: &mut Config| {
        cfg.loglevel = Some(LogLevel::new(selected.to_owned()));
    });
    // Redraw main screen
    screen.pop_layer();
    show_main_window(screen);
}
