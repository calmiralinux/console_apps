/*!
Module for serialize and deserialize config files for `vcsetup`
*/

use std::{
    fs, path::PathBuf, io::Write
};

use anyhow::{Result, Context, anyhow};
use serde::{Serialize, Deserialize};
use toml;

use crate::constants;

// TODO: add methods for serialize!

/// Конфигурационный файл программы
///
/// ## Структура конфига
///
/// ```ignore
/// loglevel = 1
/// lang = "ru_RU.UTF-8"
/// lc_lang = "en_US.UTF-8"
/// font = "cyr-sun16"
/// keymap = "ruwin_alt_sh-UTF-8"
/// ```
#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    // Fields implemented as own types to avoid panic exceptions inside UI
    pub loglevel: Option<LogLevel>,
    pub lang: Option<Lang>,
    pub lc_lang: Option<LClang>,
    pub font: Option<Font>,
    pub keymap: Option<KeyMap>,
}

impl Default for Config {
    /// Возвращает значения по умолчанию
    fn default() -> Self {
        Self {
            loglevel: Some(LogLevel::default()),
            lang: Some(Lang::default()),
            lc_lang: Some(LClang::default()),
            font: Some(Font::default()),
            keymap: Some(KeyMap::default())
        }
    }
}

pub trait ConfigProvider {
    /// Read the file and returns a string with file content
    fn read(&self) -> Result<String>;
    fn write(&self, data: &str) -> Result<()>;
}

/// Absolute path to config
pub struct ConfigPath(pub PathBuf);

impl ConfigProvider for ConfigPath {
    fn read(&self) -> Result<String> {
        fs::read_to_string(&self.0)
            .map_err(|why| anyhow!("{}", why))
    }

    fn write(&self, data: &str) -> Result<()> {
        let mut file = fs::File::create(&self.0)?;
        file.write_all(data.as_bytes())
            .with_context(|| "Can't write configuration file")?;

        Ok(())
    }
}

impl Config {
    pub fn parse_conf(data_prov: &dyn ConfigProvider) -> Result<Self> {
        data_prov.read().map(|cfg| {
            toml::from_str(&cfg)
                .with_context(|| "Can't parse configuration file")
        })?
    }

    pub fn write_conf(&self, data_prov: &dyn ConfigProvider) -> Result<()> {
        data_prov.write(
        &toml::to_string(self)
            .with_context(|| "Can't serialize configuration")?
        )
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LogLevel(u8);

impl Default for LogLevel {
    /// Возвращает значения по умолчанию
    fn default() -> Self {
        Self(constants::DEFAULT_LOGLEVEL)
    }
}

impl LogLevel {
    pub fn new(value: u8) -> Self {
        Self(value)
    }

    pub fn to_string(&self) -> String {
        format!("{}", self.0)
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Lang(String);

impl Default for Lang {
    /// Возвращает значения по умолчанию
    fn default() -> Self {
        Self(constants::DEFAULT_LANGUAGE.to_owned())
    }
}

impl Lang {
    pub fn new(value: String) -> Self {
        Self(value)
    }

    pub fn to_string(&self) -> String {
        self.0.clone()
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LClang(String);

impl Default for LClang {
    /// Возвращает значения по умолчанию
    fn default() -> Self {
        Self(constants::DEFAULT_LC_LANGUAGE.to_owned())
    }
}

impl LClang {
    pub fn new(value: String) -> Self {
        Self(value)
    }

    pub fn to_string(&self) -> String {
        self.0.clone()
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Font(String);

impl Default for Font {
    /// Возвращает значения по умолчанию
    fn default() -> Self {
        Self(constants::DEFAULT_FONT.to_owned())
    }
}

impl Font {
    pub fn new(value: String) -> Self {
        Self(value)
    }

    pub fn to_string(&self) -> String {
        self.0.clone()
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct KeyMap(String);

impl Default for KeyMap {
    /// Возвращает значения по умолчанию
    fn default() -> Self {
        Self(constants::DEFAULT_KEYMAP.to_owned())
    }
}

impl KeyMap {
    pub fn new(value: String) -> Self {
        Self(value)
    }

    pub fn to_string(&self) -> String {
        self.0.clone()
    }
}
