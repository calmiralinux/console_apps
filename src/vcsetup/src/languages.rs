use std::process::Command;

use anyhow::anyhow;

pub fn get_languages() -> Vec<String> {
    if let Ok(output) = run_locale() {
        output.lines()
            .into_iter()
            .map(|s| s.to_owned())
            .collect()
    } else {
        Vec::new()
    }
}

fn run_locale() -> anyhow::Result<String> {
    let output = Command::new("localedef")
        .arg("--list-archive")
        .output()?;

    if output.status.success() {
        let out = String::from_utf8(output.stdout)?;
        Ok(out)
    } else {
        return Err(anyhow!("Retrieve locale error."));
    }
}
