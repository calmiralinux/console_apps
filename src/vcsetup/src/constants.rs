/*!
vcsetup constants.
 */

pub const VERSION: &str = env!("CARGO_PKG_VERSION");

pub const DEFAULT_LOGLEVEL: u8 = 1;
pub const DEFAULT_LANGUAGE: &str = "ru_RU.UTF-8";
pub const DEFAULT_LC_LANGUAGE: &str = "ru_RU.UTF-8";
pub const DEFAULT_FONT: &str = "cyr-sun16";
pub const DEFAULT_KEYMAP: &str = "ruwin_alt_sh-UTF-8";

pub const CONFIG_PATH: &str = "/etc/sysconfig/console";

// Documentation
pub const DOC_REPO: &str = "https://gitlab.com/calmiralinux/documentation";
pub const HBOOK: &str = "https://gitlab.com/calmiralinux/documentation/handbook";
pub const SHORTS: &str = r#"<q>   - exit,
<Esc> - cancel,
<F1>  - select menubar"#;
