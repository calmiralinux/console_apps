use std::fs::File;
use std::io::{Error, ErrorKind, Read, Result};
use toml::Value;

use cursive::{
    event::Key,
    menu,
    views::{Dialog, TextView},
};

fn read(fname: &str) -> Result<String> {
    let mut f = File::open(fname).map_err(|err| {
        Error::new(
            ErrorKind::Other,
            format!("Can't open file '{}' cause: {}", fname, err),
        )
    })?;
    let mut text: String = String::new();

    f.read_to_string(&mut text)?;
    Ok(text)
}

fn parse(fname: &str) -> Result<toml::Value> {
    let content = read(fname)?;
    content.parse::<Value>().map_err(|_| {
        Error::new(
            ErrorKind::Other,
            format!("Failed to parse the file '{}'", fname),
        )
    })
}

fn main() -> Result<()> {
    let data = parse("/etc/calm-release")?;
    let params = ["description", "arch", "edition", "build_date"];
    let mut text: String = String::new();

    for name in params {
        let nm = data["system"][&name].as_str();
        match nm {
            Some(x) => text += &("\n".to_owned() + name + ": " + x),
            None => text += &("\n".to_owned() + name + ": unknown!!!"),
        }
    }

    let online_help = "https://gitlab.com/calmiralinux/book";
    let offline_help = "/usr/share/doc/calmira";

    let mut siv = cursive::default();

    siv.menubar()
        .add_subtree("File", menu::Tree::new().leaf("Quit", |s| s.quit()))
        .add_subtree(
            "Help",
            menu::Tree::new()
                .leaf("Online", |s| {
                    let text = "Go to the ".to_owned() + online_help + " page.";
                    s.add_layer(Dialog::info(text))
                })
                .leaf("Local documentation", |s| {
                    let text = "Go to the ".to_owned() + offline_help + " directory.";
                    s.add_layer(Dialog::info(text))
                })
                .leaf("About", |s| {
                    s.add_layer(Dialog::info("Calmira GNU/Linux-libre"))
                }),
        )
        .add_delimiter();

    siv.add_global_callback(Key::F1, |s| s.select_menubar());

    siv.add_layer(
        Dialog::around(TextView::new(&text))
            .title("About")
            .button("Quit", |s| s.quit()),
    );
    siv.run();

    Ok(())
}
