/*!
Module for configure languages in the '/etc/locale.conf'
*/

use std::{
    fs::File,
    io::{Read, Write},
    path::Path,
};

use anyhow::{anyhow, Result};

const DEFAULT_LANG: &str = "en_GB.UTF-8";

/// Create or update 'locale.conf' with given languages.
pub fn set_language(language: Option<String>, lc_lang: Option<String>) -> Result<()> {
    let mut lang = match language {
        Some(lng) => lng,
        None => DEFAULT_LANG.to_owned(),
    };

    // Double check if 'language' not contains None
    if lang == "None" {
        lang = DEFAULT_LANG.to_owned();
    }

    // Use main language if LC value not provided
    let lc_lang = match lc_lang {
        Some(lng) => lng,
        None => lang.clone(),
    };

    let path = Path::new("/etc/locale.conf");
    let config = get_config(path)?;

    let updated_config = update_config(&config, &lang, &lc_lang);

    let mut file = File::create(path)?;
    file.write_all(updated_config.as_bytes())
        .map_err(|why| anyhow!("{}", why))
}

// Update locali in the provided config.
// Create new if given config is empty.
fn update_config(config: &str, lang: &str, lc_lang: &str) -> String {
    if config.is_empty() {
        new_config(lang, lc_lang)
    } else {
        parse_config(config, lang, lc_lang)
    }
}

// Create new locale config.
fn new_config(lang: &str, lc_lang: &str) -> String {
    let mut config = String::new();
    config.push_str(&format!("LANG={}\n", lang));
    config.push_str(&format!("LC_ADDRESS={}\n", lc_lang));
    config.push_str(&format!("LC_IDENTIFICATION={}\n", lc_lang));
    config.push_str(&format!("LC_MEASUREMENT={}\n", lc_lang));
    config.push_str(&format!("LC_MONETARY={}\n", lc_lang));
    config.push_str(&format!("LC_NAME={}\n", lc_lang));
    config.push_str(&format!("LC_NUMERIC={}\n", lc_lang));
    config.push_str(&format!("LC_PAPER={}\n", lc_lang));
    config.push_str(&format!("LC_TELEPHONE={}\n", lc_lang));
    config.push_str(&format!("LC_TIME={}", lc_lang));

    config
}

// Replace locale for existing config.
fn parse_config(cfg: &str, lang: &str, lc_lang: &str) -> String {
    // Transform locale.conf into (key, value) array
    let config_pairs = cfg
        .lines()
        .into_iter()
        // Split string into key=values and replace language
        // TODO: Add in the future separate language for LANG and LC_
        .map(|s| {
            let pair = s.split_once('=').unwrap_or_default();
            if pair.0.contains("LC_") {
                (pair.0.to_owned(), lc_lang.to_owned())
            } else {
                (pair.0.to_owned(), lang.to_owned())
            }

        })
        .collect::<Vec<(String, String)>>();

    let mut config = String::new();
    for line in config_pairs.iter() {
        config.push_str(&format!("{}={}\n", line.0.to_owned(), line.1.to_owned()));
    }

    config
}

// Open given file.
//
// Return file content or empty string if file not exists
fn get_config(path: &Path) -> Result<String> {
    let mut config = String::new();

    if path.exists() {
        let mut file = File::open(path)?;
        file.read_to_string(&mut config)?;
    }

    Ok(config)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_new_config_one_lang() {
        let lang = "en_GB.UTF-8";
        let lc_lang = "en_GB.UTF-8";

        let expected = r#"LANG=en_GB.UTF-8
LC_ADDRESS=en_GB.UTF-8
LC_IDENTIFICATION=en_GB.UTF-8
LC_MEASUREMENT=en_GB.UTF-8
LC_MONETARY=en_GB.UTF-8
LC_NAME=en_GB.UTF-8
LC_NUMERIC=en_GB.UTF-8
LC_PAPER=en_GB.UTF-8
LC_TELEPHONE=en_GB.UTF-8
LC_TIME=en_GB.UTF-8"#;

        let result = new_config(&lang, &lc_lang);
        assert_eq!(result, expected);
    }

    #[test]
    fn test_new_config_multi_lang() {
        let lang = "ru_RU.UTF-8";
        let lc_lang = "en_GB.UTF-8";

        let expected = r#"LANG=ru_RU.UTF-8
LC_ADDRESS=en_GB.UTF-8
LC_IDENTIFICATION=en_GB.UTF-8
LC_MEASUREMENT=en_GB.UTF-8
LC_MONETARY=en_GB.UTF-8
LC_NAME=en_GB.UTF-8
LC_NUMERIC=en_GB.UTF-8
LC_PAPER=en_GB.UTF-8
LC_TELEPHONE=en_GB.UTF-8
LC_TIME=en_GB.UTF-8"#;

        let result = new_config(&lang, &lc_lang);
        assert_eq!(result, expected);
    }

    #[test]
    fn test_parse_config_one_lang() {
        let lang = "ru_RU.UTF-8";
        let lc_lang = "ru_RU.UTF-8";
        let cfg = r#"LANG=en_GB.UTF-8
LC_ADDRESS=en_GB.UTF-8
LC_IDENTIFICATION=en_GB.UTF-8
LC_MEASUREMENT=en_GB.UTF-8
LC_MONETARY=en_GB.UTF-8
LC_NAME=en_GB.UTF-8
LC_NUMERIC=en_GB.UTF-8
LC_PAPER=en_GB.UTF-8
LC_TELEPHONE=en_GB.UTF-8
LC_TIME=en_GB.UTF-8"#;

        let expected = r#"LANG=ru_RU.UTF-8
LC_ADDRESS=ru_RU.UTF-8
LC_IDENTIFICATION=ru_RU.UTF-8
LC_MEASUREMENT=ru_RU.UTF-8
LC_MONETARY=ru_RU.UTF-8
LC_NAME=ru_RU.UTF-8
LC_NUMERIC=ru_RU.UTF-8
LC_PAPER=ru_RU.UTF-8
LC_TELEPHONE=ru_RU.UTF-8
LC_TIME=ru_RU.UTF-8
"#;

        let result = parse_config(&cfg, &lang, &lc_lang);
        assert_eq!(result, expected);
    }

    #[test]
    fn test_parse_config_multi_lang() {
        let lang = "ru_RU.UTF-8";
        let lc_lang = "en_GB.UTF-8";
        let cfg = r#"LANG=en_GB.UTF-8
LC_ADDRESS=en_GB.UTF-8
LC_IDENTIFICATION=en_GB.UTF-8
LC_MEASUREMENT=en_GB.UTF-8
LC_MONETARY=en_GB.UTF-8
LC_NAME=en_GB.UTF-8
LC_NUMERIC=en_GB.UTF-8
LC_PAPER=en_GB.UTF-8
LC_TELEPHONE=en_GB.UTF-8
LC_TIME=en_GB.UTF-8"#;

        let expected = r#"LANG=ru_RU.UTF-8
LC_ADDRESS=en_GB.UTF-8
LC_IDENTIFICATION=en_GB.UTF-8
LC_MEASUREMENT=en_GB.UTF-8
LC_MONETARY=en_GB.UTF-8
LC_NAME=en_GB.UTF-8
LC_NUMERIC=en_GB.UTF-8
LC_PAPER=en_GB.UTF-8
LC_TELEPHONE=en_GB.UTF-8
LC_TIME=en_GB.UTF-8
"#;

        let result = parse_config(&cfg, &lang, &lc_lang);
        assert_eq!(result, expected);
    }
}