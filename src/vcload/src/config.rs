/*!
Module for deserialize config file for `vcload`
*/

use std::{
    fs, path::PathBuf
};

use serde::{Serialize, Deserialize};
use toml;
use anyhow::{Result, Context, anyhow};

// TODO: add methods for serialize!

/// Конфигурационный файл программы
///
/// ## Структура конфига
///
/// ```ignore
/// loglevel = 1
/// lang = "en_GB.UTF-8"
/// lc_lang = "ru_RU.UTF-8"
/// font = "cyr-sun16"
/// keymap = "ruwin_alt_sh-UTF-8"
/// ```
#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub loglevel: Option<u8>,
    pub lang: Option<String>,
    pub lc_lang: Option<String>,
    pub font: Option<String>,
    pub keymap: Option<String>,
}

pub trait ConfigProvider {
    /// Read the file and returns a string with file content
    fn read(&self) -> Result<String>;
}

/// Absolute path to config
pub struct ConfigPath(pub PathBuf);

impl ConfigProvider for ConfigPath {
    fn read(&self) -> Result<String> {
        fs::read_to_string(&self.0)
            .map_err(|why| anyhow!("{}", why))
    }
}

impl Config {
    pub fn parse_conf(data_prov: &dyn ConfigProvider) -> Result<Self> {
        data_prov.read().map(|cfg| {
            toml::from_str(&cfg)
                .with_context(|| "Can't parse configuration file")
        })?
    }
}

#[cfg(test)]
mod test {
    use super::*;

    struct TestConf(&'static str);

    impl ConfigProvider for TestConf {
        fn read(&self) -> Result<String> {
            Ok(self.0.to_string())
        }
    }

    #[test]
    fn test_config_ok() {
        let cfg = r#"
        loglvel = 1
        lang = "ru_RU.UTF-8"
        lc_lang = "ru_RU.UTF-8"
        font = "cyr-sun16"
        keymap = "ruwin_alt_sh-UTF-8"
        "#;

        let result = Config::parse_conf(&TestConf(cfg));
        assert!(result.is_ok(), "Configuratuion wasn't correct.");
    }

    #[test]
    fn test_config_incorrect() {
        let cfg = r#"
        loglvel = 1
        lang = ru_RU.UTF-8
        lc_lang = "ru_RU.UTF-8"
        font = "cyr-sun16"
        keymap = "ruwin_alt_sh-UTF-8"
        "#;

        let result = Config::parse_conf(&TestConf(cfg));
        assert!(result.is_err());
    }

    struct NoConf(&'static str);

    impl ConfigProvider for NoConf {
        fn read(&self) -> Result<String> {
            Err(anyhow!("No such file or directory (os error 2)"))
        }
    }

    #[test]
    fn test_nofile() {
        let result = Config::parse_conf(&NoConf(""));
        assert!(result.is_err());
    }
}