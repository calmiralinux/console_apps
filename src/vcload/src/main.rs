use std::path::PathBuf;

use anyhow::anyhow;

use vcload::config::{Config, ConfigPath};
use vcload::language::set_language;
use vcload::*;

const CONFIG_PATH: &str = "/etc/sysconfig/console";

fn main() -> anyhow::Result<()> {
    let cfg = Config::parse_conf(&ConfigPath(PathBuf::from(CONFIG_PATH)))?;

    match set_language(cfg.lang, cfg.lc_lang) {
        Ok(_) => println!("Prepare /etc/locale.conf... Done"),
        Err(why) => return Err(anyhow!("Set language: {}", why.to_string())),
    }

    match set_font(cfg.font) {
        Ok(_) => println!("Set console font... Done."),
        Err(why) => return Err(anyhow!("Set console font: {}", why.to_string())),
    }

    match set_keymap(cfg.keymap) {
        Ok(_) => println!("Set console keymap... Done."),
        Err(why) => return Err(anyhow!("Set console keymap: {}", why.to_string())),
    }

    match set_loglevel(cfg.loglevel) {
        Ok(_) => println!("Set Kernel log level... Done."),
        Err(why) => return Err(anyhow!("Set Kernel log level: {}", why.to_string())),
    }

    Ok(())
}