pub mod constants;
pub mod config;
pub mod language;

use std::process::{Command, Stdio};

use anyhow::{Result, anyhow};

pub fn set_font(font: Option<String>) -> Result<i32> {
    let Some(font) = font else {
        return Err(anyhow!("Cannot set font: {:?}", font));
    };

    run_cmd("setfont", &vec![&font])
}

pub fn set_loglevel(loglevel: Option<u8>) -> Result<i32> {
    let Some(loglevel) = loglevel else {
        return Err(anyhow!("Cannot set loglevel: {:?}", loglevel));
    };

    run_cmd("dmesg", &vec!["-n", &loglevel.to_string()])
}

pub fn set_keymap(keymap: Option<String>) -> Result<i32> {
    let Some(keymap) = keymap else {
        return Err(anyhow!("Cannot set font: {:?}", keymap));
    };

    run_cmd("loadkeys", &vec![&format!("{}.map", keymap)])
}

fn run_cmd(cmd: &str, args: &Vec<&str>) -> Result<i32> {
    let output = Command::new(cmd)
        .args(args)
        .stdout(Stdio::piped())
        .output()?;

    if !output.status.success() {
        return Err(anyhow!("{}", String::from_utf8(output.stderr)?));
    }

    Ok(output.status.code().unwrap_or_default())
}