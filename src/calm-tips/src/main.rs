use rand::prelude::*;
use walkdir::WalkDir;
use std::fs::{File, metadata};
use std::io::{BufReader, BufRead};

fn print_frame() {
    let mut i = 0;
    let k = 79;

    while i <= k {
        print!("-");
        i += 1;
    }
    println!("");
}

fn get_flist() -> std::io::Result<Vec<String>> {
    let mut flist: Vec<String> = Vec::new();
    for entry in WalkDir::new("/usr/share/doc/calmira/tips")
                    .follow_links(false)
                    .into_iter()
                    .filter_map(|e| e.ok()) {
                        let fname = entry.path().to_string_lossy();
                        let md = metadata(&*fname)?;

                        if md.is_dir() {
                            continue;
                        }

                        let _str: String = String::from(&*fname);
                        flist.push(_str);
                    }
    Ok(flist)
}

fn main() -> std::io::Result<()> {
    let flist = get_flist()?;
    let mut rng = thread_rng();

    let len = flist.len() - 1;
    let i = rng.gen_range(0..len);

    let file = &flist[i];
    
    let f = File::open(&file)?;
    let buf = BufReader::new(f);

    print_frame();
    for line in buf.lines() {
        println!("{}", line?);
    }
    print_frame();

    println!("");
    Ok(())
}
